#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/types.h>

int main(int argc, char *argv[])
{
    FILE *f;
    char ch;

    if (argc < 2) {
        printf("Missing argument\n");
        exit(EXIT_FAILURE);
    }

    printf("Hello world\n");
    f = fopen(argv[1], "r");

    if (f == NULL) {
        perror("Cannot open file");
        exit(EXIT_FAILURE);
    }

    printf("uid : %d, gid : %d, euid : %d, egid : %d\n",getuid(),getgid(),geteuid(),getegid());

    printf("File opens correctly :\n");
    while((ch = fgetc(f)) != EOF)
      printf("%c", ch);

    fclose(f);

    exit(EXIT_SUCCESS);
}